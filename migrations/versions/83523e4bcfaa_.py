"""empty message

Revision ID: 83523e4bcfaa
Revises: 3085cc295629
Create Date: 2020-01-10 15:16:04.844479

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '83523e4bcfaa'
down_revision = '3085cc295629'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('post', sa.Column('imageUrl', sa.String(length=100), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('post', 'imageUrl')
    # ### end Alembic commands ###
