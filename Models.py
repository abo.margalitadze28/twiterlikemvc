from sqlalchemy import ForeignKey

from extension import db
from flask_login import UserMixin


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(1000))
    surname = db.Column(db.String(1000))
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    birthday = db.Column(db.String(1000))
    profilepicture = db.Column(db.String(1000))


class post(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(100000))
    createDate = db.Column(db.Time)
    user_id = db.Column(db.Integer, ForeignKey('user.id'))
    imageUrl = db.Column(db.String(100))
    likes = db.relationship("Post_like", backref="post", lazy='dynamic')

class Request(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sender_id = db.Column(db.Integer,ForeignKey('user.id'))
    receiver_id = db.Column(db.Integer,ForeignKey('user.id'))
    answer = db.Column(db.Boolean, unique=False)


class User_Friends(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user1_id = db.Column(db.Integer, ForeignKey('user.id'))
    user2_id = db.Column(db.Integer, ForeignKey('user.id'))


class Post_like(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    post_id = db.Column(db.Integer, ForeignKey('post.id'))
    user_id = db.Column(db.Integer, ForeignKey('user.id'))
    likeDate = db.Column(db.Time)

class Share_post(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(100000))
    post_id = db.Column(db.Integer, ForeignKey('post.id'))
    post_user_id = db.Column(db.Integer, ForeignKey('user.id'))
    user_id=db.Column(db.Integer, ForeignKey('user.id'))
    shareDate = db.Column(db.Time)

class messege(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(100000))
    receiver_id = db.Column(db.Integer,  ForeignKey('user.id'))
    sender_id = db.Column(db.Integer, ForeignKey('user.id'))
    sendDate = db.Column(db.Time)
