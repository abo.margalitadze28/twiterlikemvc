from Models import User
from auth import auth as auth_blueprint
from extension import db
from flask_login import LoginManager
from flaskapp import app
from main import main as main_blueprint

def create_app():
    db.init_app(app)

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    with app.app_context():
        db.create_all()

    app.register_blueprint(auth_blueprint)
    app.register_blueprint(main_blueprint)


    return app


if __name__ == '__main__':
    app = create_app()
    app.run(port=5002)




