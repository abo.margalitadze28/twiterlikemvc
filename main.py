from flask import redirect
from flask import url_for
from flask_login import login_required, current_user
from flask import Blueprint, render_template, request
from datetime import datetime

from sqlalchemy import func
from sqlalchemy import or_

from sqlalchemy import case
from sqlalchemy.orm import session
from werkzeug.utils import secure_filename

from Models import post, User, Request, User_Friends, Post_like, Share_post, messege
from extension import db
from forms import PostFrom, SearchFrom, UserWallViewModel, messegeForm

main = Blueprint('main', __name__)


@main.route('/')
def index():
    form = SearchFrom()

    return render_template("index.html", form=form)


@main.route('/profile')
@login_required
def profile():
    userpost =post.query.filter_by(user_id=current_user.get_id())
    sharepost = Share_post.query.filter_by(user_id=current_user.get_id())

    share_post_info=[]

    for posts in sharepost:
        post_info=db.session.query(Share_post, User).filter_by(user_id=current_user.get_id()).join(User,
                                                                                        User.id == posts.user_id).first()
        share_post_info.append(post_info)

    form = PostFrom()
    return render_template('profile.html', form=form, name=current_user.name, userpost=userpost,share_post_info=share_post_info)


@main.route('/profile', methods=['POST'])
@login_required
def user_post():
    form = PostFrom()
    userpost = post.query.filter_by(user_id=current_user.get_id())
    sharepostforprofile = Share_post.query.filter_by(user_id=current_user.get_id())
    if form.validate_on_submit():
        content = request.form.get('content')
        time = datetime.now()
        user_id = current_user.get_id()

        filename = secure_filename(form.image.data.filename)
        form.image.data.save('static/img/' + filename)
        imageUrl = 'static/img/' + filename

        new_post = post(content=content, createDate=time, user_id=user_id,imageUrl=imageUrl)
        db.session.add(new_post)
        db.session.commit()
        return redirect(url_for('main.profile'))

    return render_template('profile.html', form=form, name=current_user.name, userpost=userpost)


@main.route('/edit_post/<string:id>', methods=['GET', 'POST'])
@login_required
def edit_post(id):
    form = PostFrom()
    userpost1 = post.query.filter_by(id=id)

    if request.method == 'GET':
        userpost = post.query.filter_by(id=id).first()
        form.content.data = userpost.content

    if request.method == 'POST' and form.validate_on_submit():
        post.query.filter_by(id=id).update(
            {
                post.content: request.form.get('content'),
                post.createDate: datetime.now(),
            }, synchronize_session=False)

        db.session.commit()
        return redirect(url_for('main.profile'))

    return render_template('edit_post.html', form=form, userpost=userpost)


@main.route('/delete_post/<string:id>', methods=['POST'])
@login_required
def delete_post(id):
    post.query.filter_by(id=id).delete()
    db.session.commit()
    return redirect(url_for('main.profile'))


@main.route('/search', methods=['POST'])
@login_required
def search():
    form = SearchFrom()
    user_email = request.form.get('shearch')

    if (user_email == '' or user_email == None):
        return render_template('index.html', form=form)

    else:
        users = User.query.filter(User.email.like('%' + user_email + '%'))

    return render_template('index.html', form=form, users=users)


@main.route('/show_user/<string:id>', methods=['GET', 'POST'])
@login_required
def show_user(id):
    user_post = post.query.filter_by(user_id=id)

    if request.method == 'POST':
        return render_template('show_user.html', user_post=user_post)

    return render_template('show_user.html')


@main.route('/request_friend/<string:id>', methods=['GET', 'POST'])
@login_required
def request_friend(id):
    form = SearchFrom()

    if request.method == 'POST':
        sender_id = current_user.get_id()
        receiver_id = id
        new_request = Request(sender_id=sender_id, receiver_id=receiver_id, answer=False)
        db.session.add(new_request)
        db.session.commit()
        return render_template('index.html', form=form)

    return render_template('index.html', form=form)


@main.route('/users_request')
def users_request():
    users_requests = Request.query.filter_by(receiver_id=current_user.get_id(), answer=False)
    all_users = []

    for users_request1 in users_requests:
        user = User.query.filter_by(id=users_request1.sender_id).first()
        if user is not None:
            all_users.append(user)

    return render_template("users_request.html", all_users=all_users)


@main.route('/Accsept_friendrequest/<string:id>', methods=['GET', 'POST'])
@login_required
def Accsept_friendrequest(id):
    form = SearchFrom()
    if request.method == 'POST':
        user1_id = current_user.get_id()
        user2_id = id
        new_User_Friends = User_Friends(user1_id=user1_id, user2_id=user2_id)
        db.session.add(new_User_Friends)
        db.session.commit()
        Request.query.filter_by(sender_id=id, receiver_id=current_user.get_id()).update(
            {
                Request.answer: True
            }, synchronize_session=False)

        db.session.commit()
    return render_template('index.html', form=form)


@main.route('/ignore_friendrequest/<string:id>', methods=['GET', 'POST'])
@login_required
def ignore_friendrequest(id):
    form = SearchFrom()
    if request.method == 'POST':
        Request.query.filter_by(sender_id=id, receiver_id=current_user.get_id()).delete()
        db.session.commit()
    return render_template('index.html', form=form)


@main.route('/users_wall')
@login_required
def users_wall():
    user_friends1 = User_Friends.query.filter_by(user2_id=current_user.get_id())
    friend_users_post1 = []


    user_friends2 = User_Friends.query.filter_by(user1_id=current_user.get_id())
    friend_users_post2 = []


    for user_post in user_friends1:
        posts = db.session.query(post, User).filter_by(user_id=user_post.user1_id).join(User,
                                                                                        User.id == user_post.user1_id)
        friend_posts = []
        if posts is not None:
            for current_post in posts:
                user_wall_view_model = UserWallViewModel()
                user_wall_view_model.postData = current_post
                like_or_unlike = Post_like.query.filter_by(user_id=current_user.get_id(),
                                                           post_id=current_post.post.id).first()
                if like_or_unlike is not None:
                    user_wall_view_model.liked = True
                else:
                    user_wall_view_model.liked = False
                friend_posts.append(user_wall_view_model)

        friend_users_post1.append(friend_posts)


    for user_post in user_friends2:
        posts = db.session.query(post, User).filter_by(user_id=user_post.user2_id).join(User,
                                                                                        User.id == user_post.user2_id)
        friend_posts1 = []
        if posts is not None:
            for current_post in posts:
                user_wall_view_model = UserWallViewModel()
                user_wall_view_model.postData = current_post
                like_or_unlike = Post_like.query.filter_by(user_id=current_user.get_id(),
                                                           post_id=current_post.post.id).first()
                if like_or_unlike is not None:
                    user_wall_view_model.liked = True
                else:
                    user_wall_view_model.liked = False
                friend_posts1.append(user_wall_view_model)

        friend_users_post2.append(friend_posts1)
    return render_template("users_wall.html", friend_users_post1=friend_users_post1,friend_users_post2=friend_users_post2)


@main.route('/share_post/<string:id>', methods=['GET', 'POST'])
@login_required
def share_post(id):
    if request.method == 'POST':
        user_share_post = post.query.filter_by(id=id).first()
        time = datetime.now()
        user_id = current_user.get_id()
        new_share_post = Share_post(content =user_share_post.content ,post_id=id, post_user_id=user_share_post.user_id, user_id=user_id,shareDate=time)
        db.session.add(new_share_post)
        db.session.commit()
    return redirect(url_for('main.profile'))



@main.route('/unshare_post/<string:id>', methods=['GET', 'POST'])
@login_required
def unshare_post(id):
    if request.method == 'POST':
        user_id = current_user.get_id()
        Share_post.query.filter_by( user_id=user_id,post_id=id).delete()
        db.session.commit()
    return redirect(url_for('main.profile'))


@main.route('/like_post/<string:id>', methods=['GET', 'POST'])
@login_required
def like_post(id):
    if request.method == 'POST':
        post_id = id
        time = datetime.now()
        user_id = current_user.get_id()
        new_like_post = Post_like(post_id=post_id, user_id=user_id, likeDate=time)
        db.session.add(new_like_post)
        db.session.commit()
        post_likes = db.session.query(Post_like, User).filter(Post_like.post_id == id) \
            .join(User, User.id == Post_like.user_id)

    return 'ok'


@main.route('/unlike_post/<string:id>', methods=['GET', 'POST'])
@login_required
def unlike_post(id):
    if request.method == 'POST':
        user_id = current_user.get_id()
        Post_like.query.filter_by(post_id=id, user_id=user_id).delete()
        db.session.commit()

    return 'okk'

@main.route('/friendlist', methods=['GET', 'POST'])
@login_required
def friendlist():


    friend_list  = []

    user_friends1 = User_Friends.query.filter_by(user2_id=current_user.get_id())
    for friend in user_friends1:
        friend_info1 = db.session.query(User_Friends, User).filter_by(user1_id=friend.user1_id).join(User,
                                                                                        User.id == friend.user1_id).first()
        friend_list.append(friend_info1)


    user_friends2 = User_Friends.query.filter_by(user1_id=current_user.get_id())
    for friend in user_friends2:
        friend_info2 = db.session.query(User_Friends, User).filter_by(user2_id=friend.user2_id).join(User,
                                                                                                    User.id == friend.user2_id).first()
        friend_list.append(friend_info2)

    return render_template("massege.html",friend_list=friend_list )


@main.route('/send_messege/<string:id>', methods=['GET', 'POST'])
@login_required
def send_messege(id):

    form=messegeForm()

    if request.method == 'POST' and form.validate_on_submit():
        content = request.form.get('content')
        time = datetime.now()
        sender_id = current_user.get_id()
        receiver_id=id
        new_messege = messege(content=content, sender_id=sender_id, receiver_id=receiver_id,sendDate=time)
        db.session.add(new_messege)
        db.session.commit()

    return render_template("send_messege.html",form=form)


@main.route('/user_messeges', methods=['GET', 'POST'])
@login_required
def user_messeges():
    usermesseges = messege.query.filter_by(receiver_id=current_user.get_id())


    return render_template("user_messeges.html",usermesseges=usermesseges)