from flask import Flask

app = Flask(__name__)

app.config['SECRET_KEY'] = '9OLWxND4o83j4K4iuopO'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://dev:dev@localhost:5431/dev'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False