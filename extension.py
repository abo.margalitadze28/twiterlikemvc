from flask_migrate import Migrate

from flask_sqlalchemy import SQLAlchemy

from flaskapp import app

db = SQLAlchemy()

migrate = Migrate(app, db)

