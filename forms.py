from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, FileField
from wtforms.validators import DataRequired


class RegisterForm(FlaskForm):
    name = StringField('name', validators=[DataRequired()])
    surname = StringField('surname', validators=[DataRequired()])
    email = StringField('email', validators=[DataRequired()])
    birthday = StringField('birthday', validators=[DataRequired()])
    password = StringField('password', validators=[DataRequired()])


class LoginForm(FlaskForm):
    email = StringField('email', validators=[DataRequired()])

    password = StringField('password', validators=[DataRequired()])


class PostFrom(FlaskForm):
    content = TextAreaField('content', validators=[DataRequired()])
    image = FileField('image', validators=[DataRequired()])

class messegeForm(FlaskForm):
    content = TextAreaField('content', validators=[DataRequired()])

class SearchFrom(FlaskForm):
    shearch = StringField('shearch', validators=[DataRequired()])


class UserWallViewModel:
    postData = []
    liked = None
