function likeUnlikePost(post_id) {


    var button = document.getElementById('post_like_unlike' + post_id);

    console.log(button.textContent.trim());

    var http = new XMLHttpRequest();
    var url;

    var buttonText = button.textContent.trim();

    console.log(buttonText);

    if (buttonText === 'like') {

        url = '/like_post/' + post_id;

        http.open('POST', url, true);

        //Send the proper header information along with the request

        http.onreadystatechange = function () {//Call a function when the state changes.
            if (http.readyState == 4) {
                if (http.status == 200) {
                    console.log(http.responseText);
                    button.textContent = 'unlike';
                } else {
                    console.log('error');
                }
            }
        };

        http.send();

    } else if (buttonText === 'unlike'){

        url = '/unlike_post/' + post_id;


        http.open('POST', url, true);

        //Send the proper header information along with the request

        http.onreadystatechange = function () {//Call a function when the state changes.
            if (http.readyState == 4) {
                if (http.status == 200) {
                    console.log(http.responseText);
                     button.textContent = 'like';
                } else {
                    console.log('error');
                }
            }
        };

        http.send();
    }


}